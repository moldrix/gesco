<?php

namespace AppBundle\Controller;
use AppBundle\Form\Type\PageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Page;

class PageController extends Controller
{

    /**
     * @Route("/page/ajouter", name="page_create")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $page = new Page();
        $formFactory = $this->get('isco.page.form.factory');
        $form = $formFactory->createForm();
        $form->setData($page);

        if ('POST' == $request->getMethod()) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->get("doctrine.orm.entity_manager");

                $em->persist($page);
                $em->flush();

                $this->get('session')->getFlashBag()->add('success', "La page a bien été créée.");
                return $this->redirect($this->generateUrl("page_create"));
            } else {
                dump($form->getErrors());
                $this->get('session')->getFlashBag()->add('error', "Le formulaire n'est pas valide");
            }
        }

        return $this->render(':Page:create.html.twig', array(
            'form' => $form->createView(),
            'page' => $page,
        ));
    }
}